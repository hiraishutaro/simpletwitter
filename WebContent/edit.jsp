<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${messages}の編集</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
        	<c:if test="${ not empty loginUser }">
    			<div class="profile">
        			<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
        			<div class="account">@<c:out value="${loginUser.account}" /></div>
        			<div class="description"><c:out value="${loginUser.description}" /></div>
    			</div>
			</c:if>

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

            <div class="form-area">
       			<form action="edit" method="post">
           			いま、どうしてる？<br />
           			<c:choose>
           				<c:when test="${ not empty errorMessages }" >
           					<textarea name="text" cols="100" rows="5" class="tweet-box">${text}</textarea>
           					<br />
           				</c:when>

           				<c:otherwise>
           					<textarea name="text" cols="100" rows="5" class="tweet-box" >${message.text}</textarea>
           					<br />
           				</c:otherwise>
           			</c:choose>

           			<input type="hidden" name="message_id" value="${message.id}" >
           			<input type="hidden" name="user_id" value="${message.userId}" >
          			<input type="submit" value="更新">（140文字まで）
       			</form>

       			<a href="./">戻る</a>
			</div>

            <div class="copyright"> Copyright(c)hirai_shutaro</div>
        </div>
    </body>
</html>